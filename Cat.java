public class Cat {
    private String name;
    private int energy;
    private int hunger;
    private int boredom;

    public Cat (String name) {
        this.name = name;
        energy = 0;
        hunger = 0;
        boredom = 0;
    }

    public boolean takeNap()
    {
        Boolean success = true;
        if (hunger > 50)
        {
            System.out.println(name+" is too hungry to nap.");
            success = false;
        }
        else if (boredom > 50)
        {
            System.out.println(name+" is too bored to nap.");
            success = false;
        }
        else
        {
            System.out.println(name+" had a lovely nap.");
            this.hunger += 5;
            this.boredom += 10;
        }
        return success;
    }

    public void scratch()
    {
        this.boredom -= 30;
        System.out.println(name=" scratched its owner.");
    }

    public boolean checkScratch(Human owner)
    {
        Boolean success = true;
        if (this.energy >= 50)
        {
            this.energy -= 50;
            scratch();
            owner.getScratched();
        }
        else
        {
            success = false;
        }
        return success;
    }
}
